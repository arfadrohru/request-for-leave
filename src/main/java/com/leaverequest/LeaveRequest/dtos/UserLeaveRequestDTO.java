package com.leaverequest.LeaveRequest.dtos;

import java.util.Date;

public class UserLeaveRequestDTO {
	private long idLeaveRequest;
	private BucketApprovalDTO bucketApproval;
	private UserDTO user;
	private Date requestDate;
	private Date leaveDateFrom;
	private Date leaveDateTo;
	private String description;
	private long remainingDays;
	private String requestStatus;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	public UserLeaveRequestDTO() {
	}
	public UserLeaveRequestDTO(long idLeaveRequest, BucketApprovalDTO bucketApproval, UserDTO user, Date requestDate,
			Date leaveDateFrom, Date leaveDateTo, String description, long remainingDays, String requestStatus,
			String createdBy, Date createdDate, String updatedBy, Date updatedDate) {
		this.idLeaveRequest = idLeaveRequest;
		this.bucketApproval = bucketApproval;
		this.user = user;
		this.requestDate = requestDate;
		this.leaveDateFrom = leaveDateFrom;
		this.leaveDateTo = leaveDateTo;
		this.description = description;
		this.remainingDays = remainingDays;
		this.requestStatus = requestStatus;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}
	public long getIdLeaveRequest() {
		return idLeaveRequest;
	}
	public void setIdLeaveRequest(long idLeaveRequest) {
		this.idLeaveRequest = idLeaveRequest;
	}
	public BucketApprovalDTO getBucketApproval() {
		return bucketApproval;
	}
	public void setBucketApproval(BucketApprovalDTO bucketApproval) {
		this.bucketApproval = bucketApproval;
	}
	public UserDTO getUser() {
		return user;
	}
	public void setUser(UserDTO user) {
		this.user = user;
	}
	public Date getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	public Date getLeaveDateFrom() {
		return leaveDateFrom;
	}
	public void setLeaveDateFrom(Date leaveDateFrom) {
		this.leaveDateFrom = leaveDateFrom;
	}
	public Date getLeaveDateTo() {
		return leaveDateTo;
	}
	public void setLeaveDateTo(Date leaveDateTo) {
		this.leaveDateTo = leaveDateTo;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getRemainingDays() {
		return remainingDays;
	}
	public void setRemainingDays(long remainingDays) {
		this.remainingDays = remainingDays;
	}
	public String getRequestStatus() {
		return requestStatus;
	}
	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
}
