package com.leaverequest.LeaveRequest.dtos;

import java.util.Date;

import com.leaverequest.LeaveRequest.models.Position;

public class UserDTO {
	private long idUser;
	private PositionDTO position;
	private String userName;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	private long leaveUsed;
	
	public UserDTO() {
	}

	public UserDTO(long idUser, PositionDTO position, String userName, String createdBy, Date createdDate,
			String updatedBy, Date updatedDate, long leaveUsed) {
		this.idUser = idUser;
		this.position = position;
		this.userName = userName;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
		this.leaveUsed = leaveUsed;
	}

	public long getIdUser() {
		return idUser;
	}

	public void setIdUser(long idUser) {
		this.idUser = idUser;
	}

	public PositionDTO getPosition() {
		return position;
	}

	public void setPosition(PositionDTO position) {
		this.position = position;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public long getLeaveUsed() {
		return leaveUsed;
	}

	public void setLeaveUsed(long leaveUsed) {
		this.leaveUsed = leaveUsed;
	}
	

	
	
}
