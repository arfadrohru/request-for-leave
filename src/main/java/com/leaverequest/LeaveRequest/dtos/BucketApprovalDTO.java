package com.leaverequest.LeaveRequest.dtos;

import java.util.Date;

public class BucketApprovalDTO {
	private long idBucketApproval;
	private UserDTO user;
	private UserLeaveRequestDTO userLeaveRequest;
	private String resolverReason;
	private Date resolvedDate;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	public BucketApprovalDTO() {
	}
	public BucketApprovalDTO(long idBucketApproval, UserDTO user, UserLeaveRequestDTO userLeaveRequest,
			String resolverReason, Date resolvedDate, String createdBy, Date createdDate, String updatedBy,
			Date updatedDate) {
		this.idBucketApproval = idBucketApproval;
		this.user = user;
		this.userLeaveRequest = userLeaveRequest;
		this.resolverReason = resolverReason;
		this.resolvedDate = resolvedDate;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}
	public long getIdBucketApproval() {
		return idBucketApproval;
	}
	public void setIdBucketApproval(long idBucketApproval) {
		this.idBucketApproval = idBucketApproval;
	}
	public UserDTO getUser() {
		return user;
	}
	public void setUser(UserDTO user) {
		this.user = user;
	}
	public UserLeaveRequestDTO getUserLeaveRequest() {
		return userLeaveRequest;
	}
	public void setUserLeaveRequest(UserLeaveRequestDTO userLeaveRequest) {
		this.userLeaveRequest = userLeaveRequest;
	}
	public String getResolverReason() {
		return resolverReason;
	}
	public void setResolverReason(String resolverReason) {
		this.resolverReason = resolverReason;
	}
	public Date getResolvedDate() {
		return resolvedDate;
	}
	public void setResolvedDate(Date resolvedDate) {
		this.resolvedDate = resolvedDate;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
}
