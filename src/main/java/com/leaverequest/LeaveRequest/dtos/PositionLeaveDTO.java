package com.leaverequest.LeaveRequest.dtos;

import java.util.Date;

public class PositionLeaveDTO {

	private long idMaster;
	private Date dateMaster;
	private long limitEmployee;
	private long limitSupervisor;
	private long limitStaff;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	public PositionLeaveDTO() {
	}
	public PositionLeaveDTO(long idMaster, Date dateMaster, long limitEmployee, long limitSupervisor, long limitStaff,
			String createdBy, Date createdDate, String updatedBy, Date updatedDate) {
		this.idMaster = idMaster;
		this.dateMaster = dateMaster;
		this.limitEmployee = limitEmployee;
		this.limitSupervisor = limitSupervisor;
		this.limitStaff = limitStaff;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}
	public long getIdMaster() {
		return idMaster;
	}
	public void setIdMaster(long idMaster) {
		this.idMaster = idMaster;
	}
	public Date getDateMaster() {
		return dateMaster;
	}
	public void setDateMaster(Date dateMaster) {
		this.dateMaster = dateMaster;
	}
	public long getLimitEmployee() {
		return limitEmployee;
	}
	public void setLimitEmployee(long limitEmployee) {
		this.limitEmployee = limitEmployee;
	}
	public long getLimitSupervisor() {
		return limitSupervisor;
	}
	public void setLimitSupervisor(long limitSupervisor) {
		this.limitSupervisor = limitSupervisor;
	}
	public long getLimitStaff() {
		return limitStaff;
	}
	public void setLimitStaff(long limitStaff) {
		this.limitStaff = limitStaff;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
}
