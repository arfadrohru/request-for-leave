package com.leaverequest.LeaveRequest.dtos;

import java.util.Date;

public class PositionDTO {
	private long idPosition;
	private String positionName;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	
	public PositionDTO() {
	}
	
	public PositionDTO(long idPosition, String positionName, String createdBy, Date createdDate, String updatedBy,
			Date updatedDate) {
		this.idPosition = idPosition;
		this.positionName = positionName;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}
	
	public long getIdPosition() {
		return idPosition;
	}
	public void setIdPosition(long idPosition) {
		this.idPosition = idPosition;
	}
	public String getPositionName() {
		return positionName;
	}
	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
	
}
