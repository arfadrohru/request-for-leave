package com.leaverequest.LeaveRequest.repositories;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import com.leaverequest.LeaveRequest.models.UserLeaveRequest;

@Repository
public interface UserLeaveRequestRepository extends JpaRepository<UserLeaveRequest, Long>, PagingAndSortingRepository<UserLeaveRequest, Long> {
	
	@Query(value = "SELECT * FROM user_leave_request where id_user = :idUser", 
			  nativeQuery = true)
			public List<UserLeaveRequest> findAllUsersWithPagination(Pageable pageable, @RequestParam("idUser") Long idUser);

}
