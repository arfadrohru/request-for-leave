package com.leaverequest.LeaveRequest.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.leaverequest.LeaveRequest.models.Position;

@Repository
public interface PositionRepository extends JpaRepository<Position, Long> {

}
