package com.leaverequest.LeaveRequest.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.couchbase.CouchbaseProperties.Bucket;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.leaverequest.LeaveRequest.dtos.BucketApprovalDTO;
import com.leaverequest.LeaveRequest.dtos.UserDTO;
import com.leaverequest.LeaveRequest.dtos.UserLeaveRequestDTO;
import com.leaverequest.LeaveRequest.exception.ResourceNotFoundException;
import com.leaverequest.LeaveRequest.models.BucketApproval;
import com.leaverequest.LeaveRequest.models.User;
import com.leaverequest.LeaveRequest.models.UserLeaveRequest;
import com.leaverequest.LeaveRequest.repositories.BucketApprovalRepository;
import com.leaverequest.LeaveRequest.repositories.UserLeaveRequestRepository;
import com.leaverequest.LeaveRequest.repositories.UserRepository;

@RestController
@RequestMapping("api/approval")
public class BucketApprovalController {

	ModelMapper modelMapper = new ModelMapper();

	@Autowired
	UserLeaveRequestRepository reqRepo;

	@Autowired
	UserRepository userRepo;

	@Autowired
	BucketApprovalRepository approvalRepo;

	@GetMapping("/findrequest") // mencari semua list dengan status waiting
	public Map<String, Object> findRequest() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<UserLeaveRequest> listStatusWaiting = mencariRequestStatusWaiting();

		List<UserLeaveRequestDTO> listRequestDto = new ArrayList<UserLeaveRequestDTO>();
		for (UserLeaveRequest request : listStatusWaiting) {
			UserLeaveRequestDTO requestDto = new UserLeaveRequestDTO();
			requestDto = modelMapper.map(request, UserLeaveRequestDTO.class);
			listRequestDto.add(requestDto);
		}

		result.put("message", listRequestDto);
		result.put("Total Data", listRequestDto.size());
		return result;
	}

	@GetMapping("/canresolve") // mencari request yang dapat di resolve oleh resolver
	public Map<String, Object> getCanIResolve(@RequestParam(name = "resolverId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Object> respone = new HashMap<String, Object>();

		List<UserLeaveRequest> listCanIresolve = mencariRequestByResolver(id, respone, true);

		List<UserLeaveRequestDTO> listRequestDto = convertEntityToDto(listCanIresolve);
		result.put("Respone", respone);
		result.put("Data", listRequestDto);
		result.put("Total Data", listRequestDto.size());
		return result;
	}

	private List<UserLeaveRequestDTO> convertEntityToDto(List<UserLeaveRequest> listCanIresolve) {
		List<UserLeaveRequestDTO> listRequestDto = new ArrayList<UserLeaveRequestDTO>();
		for (UserLeaveRequest requestCanIResolve : listCanIresolve) {
			UserLeaveRequestDTO requestDto = new UserLeaveRequestDTO();
			requestDto = modelMapper.map(requestCanIResolve, UserLeaveRequestDTO.class);
			listRequestDto.add(requestDto);
		}
		return listRequestDto;
	}

	private List<UserLeaveRequest> mencariRequestByResolver(Long id, Map<String, Object> respone,
			boolean responeAktif) {
		String resolverPosition = mencariPosisiResolver(id);
		List<UserLeaveRequest> listWaiting = mencariRequestStatusWaiting();
		List<UserLeaveRequest> listRequestCanResolve = new ArrayList<UserLeaveRequest>();
		int aktif = 0;
		for (UserLeaveRequest request : listWaiting) {
			String requestPosition = request.getUser().getPosition().getPositionName();
			if (resolverPosition.equalsIgnoreCase("SuperVisor")) {
				if (requestPosition.equalsIgnoreCase("Employee")
						|| (requestPosition.equalsIgnoreCase("Supervisor") && request.getUser().getIdUser() != id)) {
					listRequestCanResolve.add(request);
					aktif = 1;
				}
			} else if (resolverPosition.equalsIgnoreCase("Staff")) {
				if (requestPosition.equalsIgnoreCase("Staff")) {
					listRequestCanResolve.add(request);
					aktif = 2;
				}
			} else {
				aktif = 3;
			}
		}

		if (responeAktif) {
			if (aktif == 1)
				respone.put("Respone", "Supervisor Tidak Bisa Resolve Request Sendiri Dan Permohonan Cuti Staff. Berikut List Request yang Bisa di Resolve Seorang Supervisor ");
			else if (aktif == 2)
				respone.put("Respone", "Staff Tidak Bisa Resolve Selain Staff dan Request Sendiri. Berikut List Request yang Bisa di Resolve Seorang Staff");
			else if (aktif == 3)
				respone.put("Respone", "Maaf Employee Tidak Bisa Resolve Request");
		}
		return listRequestCanResolve;
	}

	private List<UserLeaveRequest> mencariRequestStatusWaiting() {
		List<UserLeaveRequest> listAllRequest = reqRepo.findAll();
		List<UserLeaveRequest> listRequestStatusWaiting = new ArrayList<UserLeaveRequest>();
		for (UserLeaveRequest request : listAllRequest) {
			if (request.getRequestStatus().equalsIgnoreCase("waiting")) {
				listRequestStatusWaiting.add(request);
			}
		}
		return listRequestStatusWaiting;
	}

	private String mencariPosisiResolver(Long id) {
		User resolver = userRepo.findById(id).get();
		String resolverPosition = "";
		if (resolver.getPosition().getPositionName().equalsIgnoreCase("Supervisor")) {
			resolverPosition = "Supervisor";
		} else if (resolver.getPosition().getPositionName().equalsIgnoreCase("Staff")) {
			resolverPosition = "Staff";
		} else if (resolver.getPosition().getPositionName().equalsIgnoreCase("Employee")) {
			resolverPosition = "Employee";
		}
		return resolverPosition;
	}

	// ======================================================================================================
	// // belum beres

	@PostMapping("/resolve")
	public Map<String, Object> resolveRequest(@RequestParam(name = "resolverId") Long resolverId,
			@RequestParam(name = "requestId") Long requestId, @RequestBody BucketApprovalDTO body)
			throws ParseException {
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Object> respone = new HashMap<String, Object>();
		Map<String, Object> data = new HashMap<String, Object>();
		UserLeaveRequest request = reqRepo.findById(requestId)
				.orElseThrow(() -> new ResourceNotFoundException("UserLeaveRequest", "idLeaveRequest", requestId));
		Date resolveDate = new Date();
		
		BucketApproval approval = new BucketApproval();
		approval = modelMapper.map(body, BucketApproval.class);
		
		boolean succes = cekApproval(resolverId, requestId, respone, data, request, resolveDate, approval, body);

		body.setIdBucketApproval(approval.getIdBucketApproval());
		UserDTO userDto = new UserDTO();
		userDto.setIdUser(resolverId);
		UserLeaveRequestDTO reqDto = new UserLeaveRequestDTO();
		reqDto.setIdLeaveRequest(requestId);
		body.setUserLeaveRequest(reqDto);
		body.setUser(userDto);
		body.setResolvedDate(resolveDate);
		if (succes) {
			respone.put("Data Apporaval", body);
		} else {
			result.put("Data", data);
		}
		result.put("Respone", respone);
		return result;
	}
	
	private boolean cekApproval(Long resolverId, Long requestId, Map<String, Object> respone, Map<String, Object> data,
			UserLeaveRequest request, Date resolveDate, BucketApproval approval, BucketApprovalDTO body) throws ParseException {
		boolean succes = false;
		Long validasitanggal = validasiTanggalResolve(request, resolveDate); // cek tanggal resolve tanggal pengajuan cuti

		UserLeaveRequest requestToFind = cekIdRequest(requestId); // cek id request yang dicari

		boolean posisiResolverSesuai = cekPosisiResolver(resolverId, respone, requestToFind, data); // walau id ketemu denmgan posisi dia bisa resolve sesuai posisi pemohon ga
		
		Long lamaCuti = lamaCuti(requestToFind);
		
		succes = validasiRespone(resolverId, requestId, respone, succes, validasitanggal, posisiResolverSesuai, requestToFind, lamaCuti, approval,resolveDate, body);
		
		return succes;
	}

	private boolean validasiRespone(Long resolverId, Long requestId, Map<String, Object> respone, boolean succes,
			Long validasitanggal, boolean posisiResolverSesuai, UserLeaveRequest requestToFind, Long lamaCuti, BucketApproval approval,  Date resolveDate, BucketApprovalDTO body) {
		if(validasitanggal <=0 ) {
			respone.put("Error : LeaveDateFrom < ResolveDate", "Kesalahan data, tanggal keputusan tidak bisa lebih awal dari pengajuan cuti.");
		}else if(posisiResolverSesuai == false) {
			mencariRequestByResolver(resolverId, respone, true);
		}else if(cekIdRequest(requestId) == null ) {
			respone.put("Error : Id Request Not Found", "Permohonan dengan ID "+requestId +" tidak ditemukan.");
		}else {
			respone.put("Resolve Succes ", "Permohonan dengan ID "+requestId +" berhasil diputuskan.");
			succes = true;
		}
		
		if(succes) {
			setEntity(requestId, requestToFind, lamaCuti, approval, resolveDate, body);	
		}
		
		return succes;
	}

	private void setEntity(Long requestId, UserLeaveRequest requestToFind, Long lamaCuti, BucketApproval approval,
			Date resolveDate, BucketApprovalDTO body) {
		
		if (body.getResolverReason().equalsIgnoreCase("Dipersilahkan")) {
			User user = userRepo.findById(requestToFind.getUser().getIdUser()).get();
			user.setLeaveUsed(user.getLeaveUsed() + lamaCuti);
			approval.setResolvedDate(resolveDate);
			approval.setResolverReason(body.getResolverReason());
			UserLeaveRequest requestEntity = new UserLeaveRequest();
			requestEntity.setIdLeaveRequest(requestId);
			approval.setUserLeaveRequest(requestEntity);
			User userEntity = new User();
			userEntity.setIdUser(requestId);
			approval.setUser(userEntity); // resolve by
			UserLeaveRequest request = reqRepo.findById(requestId).get();
			request.setBucketApproval(approval);
			request.setRequestStatus("Approved");
			approvalRepo.save(approval);
		} else {
			approval.setResolvedDate(resolveDate);
			approval.setResolverReason(body.getResolverReason());
			UserLeaveRequest requestEntity = new UserLeaveRequest();
			requestEntity.setIdLeaveRequest(requestId);
			approval.setUserLeaveRequest(requestEntity);
			User userEntity = new User();
			userEntity.setIdUser(requestId);
			approval.setUser(userEntity); // resolve by
			UserLeaveRequest request = reqRepo.findById(requestId).get();
			request.setBucketApproval(approval);
			request.setRequestStatus("Rejected");
			approvalRepo.save(approval);
		}
	}

	private Long lamaCuti(UserLeaveRequest requestToFind) {
		Long lamaCutiInMS = requestToFind.getLeaveDateTo().getTime() - requestToFind.getLeaveDateFrom().getTime();
		Long lamaCuti = TimeUnit.MILLISECONDS.toDays(lamaCutiInMS);
		return lamaCuti;
	}

	private boolean cekPosisiResolver(Long resolverId, Map<String, Object> respone, UserLeaveRequest requestToFind, Map<String, Object> data ) {
		boolean posisiResolverSesuai = false;
		String posisi = "";
		List<UserLeaveRequest> listCanResolve = mencariRequestByResolver(resolverId, respone, false);
		for (UserLeaveRequest requestCek : listCanResolve) {
			posisi = requestToFind.getUser().getPosition().getPositionName();
			if (requestCek.getIdLeaveRequest() == requestToFind.getIdLeaveRequest()) {
				if(requestToFind.getUser().getIdUser() == resolverId && requestToFind.getUser().getPosition().getPositionName().equalsIgnoreCase("Supervisor")) {
					
					posisiResolverSesuai = false;
				}
				else {
					posisiResolverSesuai = true;
				}
				
			}
		}
		if(posisiResolverSesuai == false) {
			List<UserLeaveRequestDTO> listRequestDto = convertEntityToDto(listCanResolve);
			if(posisi != "") 
				data.put("List Can Resolve", listRequestDto);
			
		}else {
			respone.put("Validasi Posisi","OK");
		}
		return posisiResolverSesuai;
	}

	private UserLeaveRequest cekIdRequest(Long requestId) {
		List<UserLeaveRequest> listStatusWaiting = mencariRequestStatusWaiting();
		UserLeaveRequest cariRequest = new UserLeaveRequest();
		for (UserLeaveRequest cekRequest : listStatusWaiting) {
			if (cekRequest.getIdLeaveRequest() == requestId) { // valdiasi id yang mau di resolve
				cariRequest = cekRequest;
			}
		}
		return cariRequest;
	}

	private Long validasiTanggalResolve(UserLeaveRequest request, Date resolveDate) throws ParseException {
		SimpleDateFormat sdf;
		sdf = new SimpleDateFormat("yyyy-MM-dd"); // ubah ke bentuk string yyyy-mm-dd
		String now = sdf.format(resolveDate);
		Date date = new SimpleDateFormat("yyyy-MM-dd").parse(now); // rubah string ke date
		Long validasiPengajuanInMS = request.getLeaveDateFrom().getTime() - date.getTime();
		Long validasiPengajuan = TimeUnit.MILLISECONDS.toDays(validasiPengajuanInMS);
		return validasiPengajuan;
	}


}
