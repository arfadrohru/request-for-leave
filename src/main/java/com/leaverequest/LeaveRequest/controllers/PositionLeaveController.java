package com.leaverequest.LeaveRequest.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.leaverequest.LeaveRequest.dtos.PositionLeaveDTO;
import com.leaverequest.LeaveRequest.exception.ResourceNotFoundException;
import com.leaverequest.LeaveRequest.models.PositionLeave;
import com.leaverequest.LeaveRequest.repositories.PositionLeaveRepository;

@RestController
@RequestMapping("api/positionleave")
public class PositionLeaveController {

	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	PositionLeaveRepository posLeRepo;
	
	@PostMapping("/create")
	public Map<String, Object> create(@Valid @RequestBody PositionLeaveDTO body){
		Map<String, Object> result = new HashMap<String, Object>();
		
		PositionLeave positionLeaveEntity = new PositionLeave();
		positionLeaveEntity = modelMapper.map(body, PositionLeave.class);
		posLeRepo.save(positionLeaveEntity);
		
		body.setIdMaster(positionLeaveEntity.getIdMaster());
		result.put("Messeage","Create PositionLeave Succes" );
		result.put("Messeage", body);
		return result;
	}
	
	@GetMapping("/read")
	public Map<String, Object> read(@Valid @RequestParam(name = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		
		PositionLeave positionLeaveEntity = posLeRepo.findById(id).orElseThrow(()-> new ResourceNotFoundException("PositionLeave", "positionLeaveId", id));
		
		PositionLeaveDTO positionLeaveDto = new PositionLeaveDTO();
		positionLeaveDto = modelMapper.map(positionLeaveEntity, PositionLeaveDTO.class);
		
		result.put("Messeage","Read PositionLeave Succes" );
		result.put("Messeage", positionLeaveDto);
		return result;
	}
	
	@GetMapping("/readall")
	public Map<String, Object> readAll(){
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<PositionLeave> listPositionLeaveEntity = posLeRepo.findAll();
		List<PositionLeaveDTO> listPositionLeaveDto = new ArrayList<PositionLeaveDTO>();
		
		for( PositionLeave positionLeaveEntity : listPositionLeaveEntity) {
			PositionLeaveDTO positionLeaveDto = new PositionLeaveDTO();
			positionLeaveDto = modelMapper.map(positionLeaveEntity, PositionLeaveDTO.class);
			listPositionLeaveDto.add(positionLeaveDto);
		}

		result.put("Messeage","Read All PositionLeave Succes" );
		result.put("Total Data", listPositionLeaveDto.size());
		result.put("Messeage", listPositionLeaveDto);
		return result;
	}
	
	@PutMapping("/update")
	public Map<String, Object> update(@Valid @RequestBody PositionLeaveDTO body, @RequestParam(name = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		
		PositionLeave positionLeaveEntity = posLeRepo.findById(id).orElseThrow(()-> new ResourceNotFoundException("PositionLeave", "positionLeaveId", id));
		String createdBy = positionLeaveEntity.getCreatedBy();
		Date createdDate = positionLeaveEntity.getCreatedDate();
		
		positionLeaveEntity = modelMapper.map(body, PositionLeave.class);
		positionLeaveEntity.setIdMaster(id);
		positionLeaveEntity.setCreatedBy(createdBy);
		positionLeaveEntity.setCreatedDate(createdDate);
		posLeRepo.save(positionLeaveEntity);
		
		body.setIdMaster(positionLeaveEntity.getIdMaster());
		result.put("Messeage","Update PositionLeave Succes" );
		result.put("Messeage", body);
		return result;
	}
	
	@DeleteMapping("/delete")
	public Map<String, Object> delete(@Valid @RequestParam(name = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		
		PositionLeave positionLeaveEntity = posLeRepo.findById(id).orElseThrow(()-> new ResourceNotFoundException("PositionLeave", "positionLeaveId", id));
		
		PositionLeaveDTO positionLeaveDto = new PositionLeaveDTO();
		positionLeaveDto = modelMapper.map(positionLeaveEntity, PositionLeaveDTO.class);
		
		posLeRepo.deleteById(id);
		
		result.put("Messeage","Delete PositionLeave Succes" );
		result.put("Messeage", positionLeaveDto);
		return result;
	}
}
