package com.leaverequest.LeaveRequest.controllers;

import java.security.Provider.Service;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.leaverequest.LeaveRequest.dtos.UserDTO;
import com.leaverequest.LeaveRequest.dtos.UserLeaveRequestDTO;
import com.leaverequest.LeaveRequest.exception.ResourceNotFoundException;
import com.leaverequest.LeaveRequest.models.PositionLeave;
import com.leaverequest.LeaveRequest.models.User;
import com.leaverequest.LeaveRequest.models.UserLeaveRequest;
import com.leaverequest.LeaveRequest.repositories.PositionLeaveRepository;
import com.leaverequest.LeaveRequest.repositories.UserLeaveRequestRepository;
import com.leaverequest.LeaveRequest.repositories.UserRepository;

@RestController
@RequestMapping("api/request")
public class UserLeaveRequestController {

	private static final String pagedResult  = null;

	ModelMapper modelMapper = new ModelMapper();

	@Autowired
	UserRepository userRepo;
	
	@Autowired
	UserLeaveRequestRepository reqRepo;

	@Autowired
	PositionLeaveRepository posLevRepo;
	

	@PostMapping("/create")
	public Map<String, Object> create(@Valid @RequestParam(name = "id") Long id,
			@RequestBody UserLeaveRequestDTO body) throws ParseException {
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Object> Respone = new HashMap<String, Object>();
		Map<String, Object> data = new HashMap<String, Object>();
		
		User userEntity = userRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("User", "userId", id));
		
		UserLeaveRequest userLeaveRequest = new UserLeaveRequest();
		Date currentDate = new Date();
		userLeaveRequest = modelMapper.map(body, UserLeaveRequest.class);
		
		userRequest(body, Respone, data, userEntity, currentDate, id, userLeaveRequest);
		
		result.put("Respone", Respone);
		result.put("Data", data);
		return result;
	}

	private void userRequest(UserLeaveRequestDTO body, Map<String, Object> Respone,Map<String, Object> data , User userEntity, Date currentDate, Long id, UserLeaveRequest userLeaveRequest) throws ParseException {
		PositionLeave masterjatahCuti = masterjatahCuti(currentDate);
		Long limitCuti = mencariJatahCuti(userEntity, masterjatahCuti);
		Long valdiasiPengajuan = validasiTanggalPengajuan(body, currentDate);
		Long lamaCuti = lamaCuti(body);
		Long sisaCuti = menghitungJatahCutiTersisa(userEntity, lamaCuti, limitCuti);
		boolean validasi = valdasiRespon(body, Respone, valdiasiPengajuan, lamaCuti, sisaCuti, currentDate, id, userLeaveRequest);
		
		setBodyEntity(body, data, userEntity, currentDate, id, userLeaveRequest, sisaCuti, validasi);
	}

	private void setBodyEntity(UserLeaveRequestDTO body, Map<String, Object> data, User userEntity, Date currentDate,
			Long id, UserLeaveRequest userLeaveRequest, Long sisaCuti, boolean validasi) {
		if (validasi) {
		setEntity(userEntity, currentDate, userLeaveRequest, sisaCuti);
		}else {
			userLeaveRequest.setRequestStatus("Pending");
		}
		data.put("Data Request", setBody(id, body, userLeaveRequest, currentDate, sisaCuti, validasi));
	}

	private void setEntity(User userEntity, Date currentDate, UserLeaveRequest userLeaveRequest, Long sisaCuti) {
		userLeaveRequest.setUser(userEntity);
		userLeaveRequest.setRequestDate(currentDate);
		userLeaveRequest.setRemainingDays(sisaCuti);
		userLeaveRequest.setRequestStatus("Waiting");
		reqRepo.save(userLeaveRequest);
	}
	
	private UserLeaveRequestDTO setBody(Long id, UserLeaveRequestDTO body, UserLeaveRequest userLeaveRequest, Date currentDate, Long sisaCuti, boolean succes) {
		body.setIdLeaveRequest(userLeaveRequest.getIdLeaveRequest());
		UserDTO userDto = new UserDTO();
		userDto.setIdUser(id);
		body.setUser(userDto);
		body.setRequestDate(currentDate);
		body.setRemainingDays(sisaCuti);
		if(succes)
		body.setRequestStatus("Waiting");
		else 
		body.setRequestStatus("Pending");
		return body;
	}
	private boolean valdasiRespon(UserLeaveRequestDTO body, Map<String, Object> Respone, Long valdiasiPengajuan,
			Long lamaCuti, Long sisaCuti, Date currentDate, Long id, UserLeaveRequest userLeaveRequest) {
		boolean succes = false;

		if(valdiasiPengajuan <=0) {
			//System.out.println("test 1");
			Respone.put("Error 1 : Cuti Backdate, tanggal pengajuan cuti < tanggal hari ini.", "Tanggal yang Anda ajukan telah lampau, silahkan ganti tanggal pengajuan cuti anda.");
		}else if(lamaCuti > 0){
			
			if(sisaCuti == 0) {
				//System.out.println("test 2");
				Respone.put("Error 4 : Jatah Cuti Habis", "Mohon maaf, jatah cuti Anda telah habis.");
			}
			else if (lamaCuti > sisaCuti) {
				//System.out.println("test 3");
				Respone.put("Error 5 : Jatah Cuti Tidak Cukup ",
						"Mohon maaf, jatah cuti Anda tidak cukup untuk digunakan dari tanggal "
								+ body.getLeaveDateFrom() + " sampai " + body.getLeaveDateTo() + " ( " + lamaCuti
								+ " hari). Jatah cuti Anda yang tersisa adalah "+ sisaCuti+" hari");
				}
			else {
				//System.out.println("test 5");
				Respone.put("Succes", "Permohonan Anda sedang diproses.");
				succes = true;
			}
		}else {
			//System.out.println("test 4");
			Respone.put("Error 2 : Tanggal Salah, leaveDateFrom > leaveDateTo ", "Tanggal yang Anda ajukan tidak valid.");
		}
		return succes;
	}

	private Long menghitungJatahCutiTersisa(User userEntity, Long lamaCuti, Long limitCuti) {
		Long sisaCuti = limitCuti - userEntity.getLeaveUsed();
		return sisaCuti;
	}

	private Long validasiTanggalPengajuan(UserLeaveRequestDTO body, Date currentDate) throws ParseException {
		SimpleDateFormat sdf;
		sdf = new SimpleDateFormat("yyyy-MM-dd"); //ubah ke bentuk string yyyy-mm-dd
		String now = sdf.format(currentDate);
		Date date = new SimpleDateFormat("yyyy-MM-dd").parse(now); // rubah string ke date
		Long validasiPengajuanInMS = body.getLeaveDateFrom().getTime() - date.getTime();
		Long validasiPengajuan = TimeUnit.MILLISECONDS.toDays(validasiPengajuanInMS);
		return validasiPengajuan;
	}

	private Long mencariJatahCuti(User userEntity, PositionLeave masterjatahCuti) {
		Long jatahCuti = (long) 0;
		if (userEntity.getPosition().getPositionName().equalsIgnoreCase("Employee")) {
			jatahCuti = masterjatahCuti.getLimitEmployee();
		} else if (userEntity.getPosition().getPositionName().equalsIgnoreCase("Supervisor")) {
			jatahCuti = masterjatahCuti.getLimitSupervisor();
		} else if (userEntity.getPosition().getPositionName().equalsIgnoreCase("Staff")) {
			jatahCuti = masterjatahCuti.getLimitStaff();
		}
		return jatahCuti;
	}

	private Long lamaCuti(UserLeaveRequestDTO body) {
		Long lamaCutiInMS = body.getLeaveDateTo().getTime() - body.getLeaveDateFrom().getTime();
		Long lamaCuti = TimeUnit.MILLISECONDS.toDays(lamaCutiInMS);
		return lamaCuti;
	}

	private PositionLeave masterjatahCuti(Date date) {// mencari jatah cuti sesuai tahun request
		List<PositionLeave> listPositionLeave = posLevRepo.findAll();
		SimpleDateFormat sdf;
		sdf = new SimpleDateFormat("yyyy");
		String paramDate = sdf.format(date);
		PositionLeave param = null;
		for (PositionLeave positionLeave : listPositionLeave) {
			String posLevDate = sdf.format(positionLeave.getDateMaster());
			if (posLevDate.equalsIgnoreCase(paramDate))
				param = positionLeave;
		}
		return param;
	}
	
	@GetMapping("/listrequest")
	public Map<String, Object> getListRequest(@RequestParam(name = "pageNo") Integer pageNo, @RequestParam(name = "pageSize") Integer pageSize, @RequestParam(name = "idUser") Long id ){
		Map<String, Object> result = new HashMap<String, Object>();

		 Pageable paging = PageRequest.of(pageNo, pageSize);
		 List<UserLeaveRequest> pagedResult = reqRepo.findAllUsersWithPagination(paging, id);
		 List<UserLeaveRequestDTO> pagedResultDto = new ArrayList<UserLeaveRequestDTO>();
		 
		 for( UserLeaveRequest request : pagedResult) {
			 UserLeaveRequestDTO requestDto = new UserLeaveRequestDTO();
			 requestDto = modelMapper.map(request, UserLeaveRequestDTO.class);
			 pagedResultDto.add(requestDto);
		 }
		 result.put("Data", pagedResultDto);
		 result.put("Total Data", pagedResultDto.size());
		 result.put("Page Size", pageSize);
		 result.put("Page Nomor", pageNo);
		 return result;
	}
	

}