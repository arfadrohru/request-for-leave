package com.leaverequest.LeaveRequest.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.leaverequest.LeaveRequest.dtos.PositionDTO;
import com.leaverequest.LeaveRequest.exception.ResourceNotFoundException;
import com.leaverequest.LeaveRequest.models.Position;
import com.leaverequest.LeaveRequest.repositories.PositionRepository;

@RestController
@RequestMapping("api/position")
public class PositionController {
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	PositionRepository posRepo;
	
	@PostMapping("/create")
	public Map<String, Object> create(@Valid @RequestBody PositionDTO body){
		Map<String, Object> result = new HashMap<String, Object>();
		
		Position positionEntity = new Position();
		positionEntity = modelMapper.map(body, Position.class);
		posRepo.save(positionEntity);
		
		body.setIdPosition(positionEntity.getIdPosition());
		result.put("Messeage","Create Position Succes" );
		result.put("Messeage", body);
		return result;
	}
	
	@GetMapping("/read")
	public Map<String, Object> read(@Valid @RequestParam(name = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		
		Position positionEntity = posRepo.findById(id).orElseThrow(()-> new ResourceNotFoundException("Position", "positionId", id));
		
		PositionDTO positionDto = new PositionDTO();
		positionDto = modelMapper.map(positionEntity, PositionDTO.class);
		
		result.put("Messeage","Read Position Succes" );
		result.put("Messeage", positionDto);
		return result;
	}
	
	@GetMapping("/readall")
	public Map<String, Object> readAll(){
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<Position> listPositionEntity = posRepo.findAll();
		List<PositionDTO> listPositionDto = new ArrayList<PositionDTO>();
		
		for( Position positionEntity : listPositionEntity) {
			PositionDTO positionDto = new PositionDTO();
			positionDto = modelMapper.map(positionEntity, PositionDTO.class);
			listPositionDto.add(positionDto);
		}

		result.put("Messeage","Read All Position Succes" );
		result.put("Total Data", listPositionDto.size());
		result.put("Messeage", listPositionDto);
		return result;
	}
	
	@PutMapping("/update")
	public Map<String, Object> update(@Valid @RequestBody PositionDTO body, @RequestParam(name = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		
		Position positionEntity = posRepo.findById(id).orElseThrow(()-> new ResourceNotFoundException("Position", "positionId", id));
		
		String createdBy = positionEntity.getCreatedBy();
		Date createdDate = positionEntity.getCreatedDate();
		
		positionEntity = modelMapper.map(body, Position.class);
		
		positionEntity.setIdPosition(id);
		positionEntity.setCreatedBy(createdBy);
		positionEntity.setCreatedDate(createdDate);
		posRepo.save(positionEntity);
		
		body.setIdPosition(positionEntity.getIdPosition());
		result.put("Messeage","Update Position Succes" );
		result.put("Messeage", body);
		return result;
	}
	
	@DeleteMapping("/delete")
	public Map<String, Object> delete(@Valid @RequestParam(name = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		
		Position positionEntity = posRepo.findById(id).orElseThrow(()-> new ResourceNotFoundException("Position", "positionId", id));
		
		PositionDTO positionDto = new PositionDTO();
		positionDto = modelMapper.map(positionEntity, PositionDTO.class);
		
		posRepo.deleteById(id);
		
		result.put("Messeage","Delete Position Succes" );
		result.put("Messeage", positionDto);
		return result;
	}

}
