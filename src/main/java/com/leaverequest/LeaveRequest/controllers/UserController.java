package com.leaverequest.LeaveRequest.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.leaverequest.LeaveRequest.dtos.UserDTO;
import com.leaverequest.LeaveRequest.exception.ResourceNotFoundException;
import com.leaverequest.LeaveRequest.models.User;
import com.leaverequest.LeaveRequest.repositories.UserRepository;

@RestController
@RequestMapping("api/user")
public class UserController {

	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	UserRepository userRepo;
	
	@PostMapping("/create")
	public Map<String, Object> create(@Valid @RequestBody UserDTO body){
		Map<String, Object> result = new HashMap<String, Object>();
		
		User userEntity = new User();
		userEntity = modelMapper.map(body, User.class);
		
		userRepo.save(userEntity);
		
		body.setIdUser(userEntity.getIdUser());
		System.out.println(userEntity.getIdUser());
		result.put("Messeage","Create User Succes" );
		result.put("Messeage", body);
		return result;
	}
	
	@GetMapping("/read")
	public Map<String, Object> read(@Valid @RequestParam(name = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		
		User userEntity = userRepo.findById(id).orElseThrow(()-> new ResourceNotFoundException("User", "userId", id));
		
		UserDTO userDto = new UserDTO();
		userDto = modelMapper.map(userEntity, UserDTO.class);
		
		result.put("Messeage","Read User Succes" );
		result.put("Messeage", userDto);
		return result;
	}
	
	@GetMapping("/readall")
	public Map<String, Object> readAll(){
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<User> listUserEntity = userRepo.findAll();
		List<UserDTO> listUserDto = new ArrayList<UserDTO>();
		
		for( User userEntity : listUserEntity) {
			UserDTO userDto = new UserDTO();
			userDto = modelMapper.map(userEntity, UserDTO.class);
			listUserDto.add(userDto);
		}

		result.put("Messeage","Read All User Succes" );
		result.put("Total Data", listUserDto.size());
		result.put("Messeage", listUserDto);
		return result;
	}
	
	@PutMapping("/update")
	public Map<String, Object> update(@Valid @RequestBody UserDTO body, @RequestParam(name = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		
		User userEntity = userRepo.findById(id).orElseThrow(()-> new ResourceNotFoundException("User", "userId", id));
		String createdBy = userEntity.getCreatedBy();
		Date createdDate = userEntity.getCreatedDate();
		
		userEntity = modelMapper.map(body, User.class);
		userEntity.setIdUser(id);
		userEntity.setCreatedBy(createdBy);
		userEntity.setCreatedDate(createdDate);
		userRepo.save(userEntity);
		
		body.setIdUser(userEntity.getIdUser());
		result.put("Messeage","Update User Succes" );
		result.put("Messeage", body);
		return result;
	}
	
	@DeleteMapping("/delete")
	public Map<String, Object> delete(@Valid @RequestParam(name = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		
		User userEntity = userRepo.findById(id).orElseThrow(()-> new ResourceNotFoundException("User", "userId", id));
		
		UserDTO userDto = new UserDTO();
		userDto = modelMapper.map(userEntity, UserDTO.class);
		
		userRepo.deleteById(id);
		
		result.put("Messeage","Delete User Succes" );
		result.put("Messeage", userDto);
		return result;
	}
}
