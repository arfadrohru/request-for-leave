package com.leaverequest.LeaveRequest.models;
// Generated Oct 20, 2020, 2:05:40 PM by Hibernate Tools 4.3.5.Final

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * PositionLeave generated by hbm2java
 */
@Entity
@Table(name = "position_leave", schema = "public")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdDate", "updatedDate", "createdBy", "updatedBy"}, allowGetters = true)
public class PositionLeave implements java.io.Serializable {

	private long idMaster;
	private Date dateMaster;
	private long limitEmployee;
	private long limitSupervisor;
	private long limitStaff;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;

	public PositionLeave() {
	}

	public PositionLeave(long idMaster, Date dateMaster, long limitEmployee, long limitSupervisor, long limitStaff,
			String createdBy, Date createdDate, String updatedBy, Date updatedDate) {
		this.idMaster = idMaster;
		this.dateMaster = dateMaster;
		this.limitEmployee = limitEmployee;
		this.limitSupervisor = limitSupervisor;
		this.limitStaff = limitStaff;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_id_position_leave_seq")
	@SequenceGenerator(name = "generator_id_position_leave_seq", sequenceName = "id_position_leave_seq", schema = "public", allocationSize = 1 )
	@Column(name = "id_master", unique = true, nullable = false)
	public long getIdMaster() {
		return this.idMaster;
	}

	public void setIdMaster(long idMaster) {
		this.idMaster = idMaster;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "date_master", nullable = false, length = 13)
	public Date getDateMaster() {
		return this.dateMaster;
	}

	public void setDateMaster(Date dateMaster) {
		this.dateMaster = dateMaster;
	}

	@Column(name = "limit_employee", nullable = false)
	public long getLimitEmployee() {
		return this.limitEmployee;
	}

	public void setLimitEmployee(long limitEmployee) {
		this.limitEmployee = limitEmployee;
	}

	@Column(name = "limit_supervisor", nullable = false)
	public long getLimitSupervisor() {
		return this.limitSupervisor;
	}

	public void setLimitSupervisor(long limitSupervisor) {
		this.limitSupervisor = limitSupervisor;
	}

	@Column(name = "limit_staff", nullable = false)
	public long getLimitStaff() {
		return this.limitStaff;
	}

	public void setLimitStaff(long limitStaff) {
		this.limitStaff = limitStaff;
	}

	@Column(name = "created_by", nullable = false)
	@CreatedBy
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "created_date", nullable = false, length = 13)
	@CreatedDate
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "updated_by", nullable = false)
	@LastModifiedBy
	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "updated_date", nullable = false, length = 13)
	@LastModifiedDate
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
