package com.leaverequest.LeaveRequest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.leaverequest.LeaveRequest.models.AuditAwareImpl;

@SpringBootApplication
@EnableJpaAuditing
public class LeaveRequestApplication {

	@Bean
	AuditorAware<String> auditorProvider() {
	    return new AuditAwareImpl();
	}
	
	public static void main(String[] args) {
		SpringApplication.run(LeaveRequestApplication.class, args);
	}
	
}
